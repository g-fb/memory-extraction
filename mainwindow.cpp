#include "mainwindow.h"

#include <QFileSystemModel>
#include <QStandardPaths>
#include <QTreeView>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    resize(300, 350);
    auto treeModel = new QFileSystemModel();
    treeModel->setFilter(QDir::Files | QDir::NoDotAndDotDot);
    treeModel->setNameFilters(QStringList() << "*.zip" << "*.7z" << "*.cbz" << "*.cbt" << "*.cbr" << "*.cb7" << "*.rar");
    treeModel->setNameFilterDisables(false);
    treeModel->setRootPath(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));

    auto treeView = new QTreeView(this);
    treeView->setModel(treeModel);
    treeView->setColumnHidden(1, true);
    treeView->setColumnHidden(2, true);
    treeView->setColumnHidden(3, true);
    treeView->setRootIndex(treeModel->index(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation)));

    connect(treeView, &QTreeView::doubleClicked, this, [=](const QModelIndex &index) {
        extractToMemory(treeModel->filePath(index));
    });

    setCentralWidget(treeView);
}

void MainWindow::extractToMemory(QString archiveFile)
{
    qDebug() << archiveFile;

    using QArchive::MemoryExtractor;
    using QArchive::MemoryExtractorOutput;
    using QArchive::MemoryFile;

    auto extractor = new MemoryExtractor(archiveFile);
    extractor->setCalculateProgress(true);
    extractor->getInfo();
    extractor->start();

    connect(extractor, &MemoryExtractor::finished, this, [=](MemoryExtractorOutput *data) {
        QVector<MemoryFile> files = data->getFiles();
        images.clear();
        images = files;
        data->deleteLater();
        extractor->clear();

        qDebug() << "MemoryExtractor::finished \n";
    });

    connect(extractor, &MemoryExtractor::error, this, [&](short code) {
        qInfo() << "An error has occured ::" << QArchive::errorCodeToString(code);
    });
}
