#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QArchive>

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() = default;
    void extractToMemory(QString archiveFile);
    QVector<QArchive::MemoryFile> images;
};
#endif // MAINWINDOW_H
